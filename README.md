# Welcome to Image #1
This works best on Mac and Linux but also works in Windows.

## First time instructions
1. Download and install [Vagrant](https://www.vagrantup.com/downloads.html)
2. Download and install the latest v5.2.x of [VirtualBox](https://www.virtualbox.org/wiki/Download_Old_Builds_5_2) for your Operating System (do not download v6.x)
3. Windows only: download and install [Git](https://git-scm.com/download). When you install, you have the option to add Git and its Linux-style command line tools to your system path, make sure you take this option!
4. Windows only: Run all subsequest commands in Git Bash, not any other terminal
5. Mac only: download and install [Git](https://hackernoon.com/install-git-on-mac-a884f0c9d32c)
6. `git clone https://gitlab.com/poflynn/CPImage1.git`
7. `cd CPImage1`
8. `vagrant up` (Expect this to take some time the first time you run it)
9. `vagrant ssh` You are now on the command line of the practice image

## When done
1. `vagrant destroy`

## To Restart From Scratch
1. `vagrant destroy`
2. `vagrant up` (note after the initial 'vagrant up', all subsequent vagrant ups of any 'images' from anyone will be very fast)

# To Create Your Own "image"
1. Using git create a new branch 
1. Put 'bad commands' in [scripts/doBadThings.sh](scripts/doBadThings.sh)
1. Edit csel.cfg to look for the bad settings screated in step 2
1. Edit the README to inform the user of the rules for the image
1. Commit and push
1. Share the branchname to the group

## Tips for Creating Your Own Images
Be sure to read the Scoring Engine's [README](https://gitlab.com/poflynn/CSEL#features).  
Be warned: I notice some 'vulnerability' checks are not working, for example ['Disable SSH Root Login'](https://gitlab.com/poflynn/CSEL/blob/master/csel.cfg#L38).  
To create vulnerabilities try reversing engineering the scoring engine, for example:  
1. [How to check for a minimum password age](https://gitlab.com/poflynn/CSEL/blob/master/payload#L386)
1. [How to check if guest login is enabled](https://gitlab.com/poflynn/CSEL/blob/master/payload#L236)

https://gitlab.com/poflynn/CSEL#csel

# Image #1 README
1. Allowed users are user1 user2 user3 user4 user5, vagrant, root
2. Add a new user called user8
3. No blank passwords are allowed for any users
4. Only vagrant is allowed to sudo
5. /home/CYBERPATRIOT_DO_NOT_REMOVE is ok
6. Sudo should timeout
7. There should be no guest login
8. Autologin should be disabled
8. Don't display all the users on the login screen
9. No MP3 or MP4 files are allowed
1. If a user types in the wrong password there should be a 3s delay before they can try again
1. Users should be allowed only 5 failed login attempts, regardless of what the PAM settings may be
1. Fix https://www.google.com (hint: don't just comment out this line, remove it)
1. You ignore any warnings about the "Virtualbox Guest Additions" being out of date

## Checking Progress
Double click on the "ScoreReport.html" file on your desktop to view your progress.  
Note that it is updated every minute so you may need to wait for your updates to be noticed.  

Enjoy!
