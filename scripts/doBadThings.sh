#!/bin/bash -eu

# Create a lot of users ("gecos" option stops the system from asking you the user's name etc)
adduser --disabled-password --gecos "" user1
adduser --disabled-password --gecos "" user2
adduser --disabled-password --gecos "" user3
adduser --disabled-password --gecos "" user4
adduser --disabled-password --gecos "" user5
adduser --disabled-password --gecos "" user6
adduser --disabled-password --gecos "" user7

# Add user2 to sudo group
usermod -aG sudo user2

# Delete passwords for users 3 and 4 (if their pwd is changed it will now be noticed)
passwd -d user3
passwd -d user4

# Disable sudo timeout
echo "Defaults:ALL timestamp_timeout=-1" >>/etc/sudoers

# Disable firewall
ufw disable

# Download mp3 to user Vagrant's Documents
curl -s http://www.sample-videos.com/audio/mp3/crowd-cheering.mp3 -o "/home/vagrant/Documents/crowd-cheering.mp3"
chown vagrant:vagrant /home/vagrant/Documents/crowd-cheering.mp3
# Copy mp3 to hidden file (by putting a "." in front of the filename) to user Vagrant's home directory (use `ls -la` to view it)
cp /home/vagrant/Documents/crowd-cheering.mp3 /home/vagrant/.crowd-cheering.mp3 
chown vagrant:vagrant /home/vagrant/.crowd-cheering.mp3 

# Download mp4 (video) file
curl -s http://www.sample-videos.com/video/mp4/480/big_buck_bunny_480p_1mb.mp4  -o "/home/vagrant/Documents/big_buck_bunny_480p_1mb.mp4"
chown vagrant:vagrant /home/vagrant/Documents/big_buck_bunny_480p_1mb.mp4
# Copy the mp4 file to user Vagrant's home but make it hidden
cp /home/vagrant/Documents/big_buck_bunny_480p_1mb.mp4 /home/vagrant/.big_buck_bunny_480p_1mb.mp4
chown vagrant:vagrant /home/vagrant/.big_buck_bunny_480p_1mb.mp4
# Copy the mp4 file to user Vagrant's Documents but sneakily change the file extension so it looks like a Word doc
cp /home/vagrant/Documents/big_buck_bunny_480p_1mb.mp4 /home/vagrant/Documents/resume.doc
chown vagrant:vagrant /home/vagrant/Documents/resume.doc

# Redirect browser requests to google to a goofy site instead
sh -c "echo '198.252.206.140 www.google.com google.com' >>/etc/hosts"

# Turn on auto-login on boot-up
sed -i "s/autologin-guest=false/autologin-guest=true/g" /etc/lightdm/lightdm.conf

# Display all users on login screen
echo "greeter-hide-users=false" >>/etc/lightdm/lightdm.conf

# If a user types the wrong password delay them for 1s (delay is in ms)
sed -i "s/delay=3000000/delay=1000000/g" /etc/pam.d/login

# Allow 9 login attempts before disabling the user
sed -i "s/LOGIN_RETRIES.*/LOGIN_RETRIES      9/g" /etc/login.defs

# Install netcat ("nc"), AKA "backdoor"
# Commented out as it didn't seem to work...
# apt-get install -qq netcat > /dev/null
